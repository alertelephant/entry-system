FROM python:3
ENV PYTHONUNBUFFERED=1
ENV APPDIR=/demoEvent
# This can be any name, chosen to name after the project dir for consistency.
# This is the directory that will house the Django application.

WORKDIR ${APPDIR}
COPY requirements.txt ${APPDIR}
# Install the requirements.
RUN pip install -r requirements.txt
# Copy over the application - this is done after requirements installation since it is the most
# likely to change (save on build steps).
COPY eventApp ${APPDIR}
# At this point we would be compelling SASS by using NPM gulp, for example.
# This is a simple demo so this part is left out.
# Collect static - this is run to collect our static assets together after gulp
# has processed them.
# This is also where we potentially scream at the frontend developers when they
# have forgotten to provide a static asset, listed in the CSS and whitenoise blows up!

# This should be run ideally as a separate process, on deployment.
RUN python manage.py collectstatic --noinput

# We cannot run migration here, DB wont be up!
# Typically migration is run on a seperate process.

# Finally run up daphne.
CMD daphne project.asgi:application -b 0.0.0.0 -p 8000