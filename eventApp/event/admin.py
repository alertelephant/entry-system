from django.contrib import admin

# Entry and entry user admin.
from event.models import Event, EventUser


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):

    pass


@admin.register(EventUser)
class EventUserAdmin(admin.ModelAdmin):

    raw_id_fields = [
        "user"
    ]
