from django.core.management.base import BaseCommand
from django.core.management import call_command
from event.models import Event


class Command(BaseCommand):
    """
    Run up the fixtures if they have not been run.
    """

    def handle(self, *args, **options) -> None:
        """
        Execute fixture running.
        :param args:
        :param options:
        :return:
        """

        # Run the fixtures?
        if Event.objects.count() > 0:
            # Already run
            return

        for fix in [
            "auth.user.json",
            "client.clientuser.json",
            "event.event.json"
        ]:
            # Execute fixture commend for each fixture
            # The command is loaddata
            call_command("loaddata", fix)
