from typing import Any
from os import environ
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model


class Command(BaseCommand):

    def handle(self, *args: Any, **options: Any) -> None:
        """
        Autogenerate an initial admin if they do not exist.
        :param args:
        :param options:
        :return:
        """

        # get the user model - always to this, this way never got to auth.User since the system could
        # be modified to use a custom user.
        user_model = get_user_model()

        admin_name = environ.get("ADMIN_SUPERUSER_NAME", None)
        admin_pass = environ.get("ADMIN_SUPERUSER_PASSWORD", None)
        admin_email = environ.get("ADMIN_EMAIL", None)

        if (admin_name or admin_pass or admin_email) is None:
            # Got an issue!
            v = vars()
            missing = [cred.replace('_', ' ').title() for cred in ['admin_name', 'admin_email', 'admin_pass'] if v[cred] is None]
            raise CommandError(f"Missing admin credentials for {missing}")
        try:
            admin = user_model.objects.get(username=admin_name)
            # Update admin password
            admin.set_password(admin_pass)
            admin.save()
        except user_model.DoesNotExist:
            user_model.objects.create_superuser(admin_name, admin_email, admin_pass)
