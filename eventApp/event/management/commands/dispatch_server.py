# This is the dispatcher that processes the new users and entries in the Redis zlist
from uuid import uuid1
from typing import Any
from json import loads
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.db import transaction
import django_redis

from event.models import Event, EventUser

conn_rd2 = django_redis.get_redis_connection("redis_2")
conn_rd3 = django_redis.get_redis_connection("redis_3")


class Command(BaseCommand):

    help = "Dispatch processor daemon."

    def handle(self, *args: Any, **options: Any):
        """
        This runs up the dispatch daemon to process the entries from redis into the DB.
        :param args:
        :param options:
        :return:
        """
        self.dispatcher()

    def dispatcher(self) -> None:
        """
        The dispatcher for processing the Redis hash list data.
        :return:
        """
        # This is not implemented as a celery/dramatiq task, since it has to rapidly process the data in the
        # Redis hash store.
        print("Event processing dispatcher running.")
        key = settings.REDIS_LIST_PROCESS_KEY
        user_model = get_user_model()

        conn_ct = 0
        cons = [conn_rd2, conn_rd3]

        # Run forever.
        while True:

            # Probe the redis servers 1 and 2 (round robin) and process our entrants into the DB.
            # Two data formats:
            # Format will be a JSON containing the keys:
            # event_id
            # email
            # first_name
            # last_name
            # password (already hashed).
            # telephone
            # user_id  - This is set if this is data from an existing user.

            conn = cons[conn_ct]

            conn_ct += 1
            if conn_ct > 1:
                conn_ct = 0

            # We have to do this, this way (can't use scan).
            h_keys = conn.hkeys(key)

            if len(h_keys) == 0:
                continue

            email = h_keys[0]

            # In one transaction - get the data then delete the key.
            pipe = conn.pipeline()
            pipe.multi()  # Build transaction.
            pipe.hget(key, email)
            pipe.hdel(key, email)
            data = pipe.execute()  # Execute transaction.

            if not data[0]:
                # Beaten to it!
                continue

            # Extract data (in index 0, index 1 holds the outcome of the key deletion)
            data = loads(data[0])
            user_id = data.get("user_id", None)

            # Get the event
            # PyCharm lint note and community edition, PyCharm will complain about missing 'objects' if you do not have
            # the Django build system.
            # noinspection PyUnresolvedReferences
            try:
                # noinspection PyUnresolvedReferences
                event = Event.objects.get(pk=data['event_id'])
            except Event.DoesNotExist:
                # No point continuing, no event to enter - see comment below.
                continue

            if event.event_expired:
                # Event has expired, for this demo this is a flaw, the user thinks they have entered, but they were
                # just too late!
                # In a real system they would have not been charged, any merchant transaction would have been
                # closed. If they got past this point and they entered, then they would be charged (future pay).
                continue

            # Generate a user model, we cannot use update_or_create since we do not have a username (it is generated).
            with transaction.atomic():
                # This has to be an atomic transaction to prevent any risk of duplication.
                if user_id:
                    # Existing user and existing event user.
                    try:
                        user = user_model.objects.get(pk=user_id)
                    except user_model.DoesNotExist:
                        # The user does not exist so we can go no further!
                        continue

                    user.first_name = data['first_name']
                    user.last_name = data['last_name']
                    user.save()

                    # Update the event user
                    ev_user = user.eventuser
                    ev_user.address_1 = data['address_1']
                    ev_user.address_2 = data['address_2']
                    ev_user.town_city = data['town_city']
                    ev_user.zip_postcode = data['zip_postcode']
                    ev_user.telephone = data['telephone']
                    ev_user.save()

                    if event not in ev_user.events.all():
                        # As an aside - you can repeatedly call add() without checking if the event has been entered,
                        # add() is idempotent. But we do not want to charge the entrant for re-entering the same event!!
                        ev_user.events.add(event)

                        # We would now charge the entrant, the EventUser model would have a transaction field,
                        # which would be passed to the merchant, together with the precalculated gross cost.
                        # Typically the merchant gateway would have passed back something like nonce which would be
                        # used to finalise the transaction.
                else:
                    # New user
                    user = user_model(
                        first_name=data['first_name'],
                        last_name=data['last_name'],
                        email=data['email'],
                        password=data['password'],  # Password is already hashed
                        username=str(uuid1())
                    )
                    user.save()

                    # Generate the event user and link to the user and event.
                    ev_user = EventUser(
                        user=user,
                        address_1=data['address_1'],
                        address_2=data['address_2'],
                        town_city=data['town_city'],
                        zip_postcode=data['zip_postcode'],
                        telephone=data['telephone']
                    )

                    ev_user.save()

                    # m2m link to this event
                    ev_user.events.add(event)

                    # We would now charge the entrant, see comment above.
