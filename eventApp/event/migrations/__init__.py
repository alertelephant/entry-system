# Note on migrations in a collaborative setting, IE using VCS like git.
# Be wary with multiple collaborators generating migrations.
# If they are working on the same models or models related together,
# you can wind up with a migration mess!!
