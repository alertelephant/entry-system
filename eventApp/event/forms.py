# Event entry forms.
from random import choice

import django_redis
from django import forms
from django.conf import settings
from json import dumps

from django.contrib.auth.hashers import make_password
from django.core.exceptions import ValidationError

from event.models import Event, EventUser

conn_rd2 = django_redis.get_redis_connection("redis_2")
conn_rd3 = django_redis.get_redis_connection("redis_3")


class EventEntryForm(forms.ModelForm):
    """
    Defines the event entry form - full entry, entry of the user.
    """

    # We need to add in the fields in the companion user table
    first_name = forms.CharField(max_length=150)
    last_name = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput(), required=False)
    confirm_password = forms.CharField(max_length=150, widget=forms.PasswordInput(), required=False)

    # Will host this user's email address for this entry.
    email: str
    event_id: int

    # This is is a model form but we will not be saving to the model! ModelForm used to rapidly pre-fill in fields.
    class Meta:
        model = EventUser
        exclude = [
            "id",
            "events",  # We fill that in, so it is excluded.
            "user"
        ]

    def __init__(self, *args, **kwargs):
        """
        Extract our arguments.
        :param args:
        :param kwargs:
        """
        self.email = kwargs.pop("email")
        self.event_id = kwargs.pop("event_id")
        self.user_id = kwargs.pop("user_id")
        
        super(EventEntryForm, self).__init__(*args, **kwargs)

        if self.user_id:
            # We remove the password fields - they are not required.
            del self.fields["password"]
            del self.fields["confirm_password"]

    def clean_password(self):
        """
        This will be called if teh password fields exist.
        :return:
        """
        data = self.cleaned_data.get("password", None)

        # If they exist they must be filled in!
        # Note, we test true and "truthy" to cover password == "" as well as None
        if not data:
            raise ValidationError("Please provide a password.")

        return data

    def clean_confirm_password(self):
        """
        As with password above we
        :return:
        """
        data = self.cleaned_data.get("confirm_password", None)

        if not data:
            raise ValidationError("Please provide a password.")

        return data

    def clean(self):
        """
        Verify passwords match (if set)
        :return:
        """
        password = self.cleaned_data.get("password", None)
        confirm_password = self.cleaned_data.get("confirm_password", None)

        # Aside - Python evaluation note, like many languages Python evaluates left to right, so 'password' being set
        # will be evaluated before 'password' inequality test.
        if password and password != confirm_password:
            # We need to raise a validation error, this will appear in the non-form field errors as apposed to appearing
            # next to the field.
            raise ValidationError("The passwords do not match!")

        # Now hash the password - we DON'T want it to be passed into Redis (or any external store), in the clear!
        self.cleaned_data["password"] = make_password(password)

    def save(self, commit=True):
        """
        We intercept save.
        :param commit:
        :return:
        """

        if not commit:
            return super(EventEntryForm, self).save(commit=commit)

        # Serialise - the data will be simple enough for JSON to handle.
        # But first we have to add some missing data, passed over to this form.
        self.cleaned_data.update({
            'event_id': self.event_id,
            'user_id': self.user_id,
            'email': self.email
        })
        # BUG: User ID == None
        data = dumps(self.cleaned_data)
        # We save to Redis for rapid processing, under the user's email.
        # We use a hash so if the same data is submitted > once, it will not be duplicated in the DB.
        # Hash is done on the email since that is unique to the user.
        # BUT! Which one?
        # For demo purposes we will choose randomly, in a production system this would be done using a cluster.
        conn = choice([conn_rd2, conn_rd3])
        conn.hset(settings.REDIS_LIST_PROCESS_KEY, self.email, data)

        # That's it! The daemon process will pick this up and process to DB in its own time.
        # In the mean time we can return to the now on-boarded user!


class LoginForm(forms.Form):
    """
    This is presented to the event entry user to start the process of login, or signup.
    """

    email = forms.EmailField(max_length=1024)
    password = forms.CharField(max_length=1024, required=False, widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        """
        Switch email to password.
        :param args:
        :param kwargs:
        """

        password_req = kwargs.pop("password_req") if 'password_req' in kwargs else None

        super(LoginForm, self).__init__(*args, **kwargs)

        if password_req:
            # Set the email field hidden and un-hide the password field (settings it mandatory).
            # This is presented to the user, if they have an existing account.
            self.fields["email"].widget = forms.HiddenInput()
            self.fields["password"].widget = forms.PasswordInput()
