from datetime import datetime, timezone
from django.db import models
from django.contrib.auth.models import AbstractUser, Group
# This event system may not be confined to a single language, so we translate.
# ** Be careful of grammar **, do not impose a grammar structure, do not use templates or
# concat strings:
# _(f"Bad example, the time is {some_time_var}")
# _("Good example, the time is %(time)") % {"time": some_time_var}
from django.urls import reverse
from django.utils.translation import gettext as _
# Example of recycling a template tag!
from django.template.defaultfilters import pluralize
from django.conf import settings


# noinspection PyUnresolvedReferences
from client.models import ClientUser

from project.utils import AddressMixin, PhoneMixin


class Event(models.Model):
    """
    Represents an event.
    """
    # I don't show annotations on variables, unless it is not obvious what type they are.
    # So you will not see from me this, for example:
    # event_name: models.CharField = models.CharField(max_length=1024, help_text=_("name of the event"))

    # Define the event name - we choose to fix this at 1024 characters.
    name = models.CharField(max_length=1024, help_text=_("name of the event"))

    # A related_name is included here since it clashes with auth.User.groups.
    client = models.ForeignKey(ClientUser, help_text=_("Client that owns this event."), on_delete=models.CASCADE)

    start_date_time = models.DateTimeField(help_text=_("Start date and time of event, times are in UTC."))
    end_date_time = models.DateTimeField(help_text=_("End date and time of event, times are in UTC."))

    close_on_start = models.BooleanField(default=False, help_text=_("Close this event when it has started."))

    @property
    def event_live(self) -> bool:
        """
        Is this event live?
        :return:
        """
        # We need a standard time zone to work from, we have chosen UTC. This needs to be aware since Django datetime
        # objects are aware.
        utc_now = datetime.now(timezone.utc)

        return self.start_date_time >= utc_now < self.end_date_time

    @property
    def event_expired(self) -> bool:
        """
        Has the event expired or has closed.
        :return:
        """
        utc_now = datetime.now(timezone.utc)

        return self.end_date_time < utc_now or (self.start_date_time < utc_now and self.close_on_start)

    def __str__(self) -> str:
        """
        Retrun representation of this object.
        :return:
        """
        return f"{self.name} for client {self.client.organisation_name}"

    def get_absolute_url(self) -> str:
        """
        Returns the URl to the login for enrolling in this event
        :return:
        """
        return reverse("event-entry-login", kwargs={'id': self.id})


class EventUser(AddressMixin, PhoneMixin, models.Model):
    """
    An event user.
    """

    # We are adding to the user database so we are *not* going to use a proxy model.
    # Ideally for security we would want to implement a separate model inheriting from AbstractUser,
    # to keep these users out of the admin part.
    # But we would then have to define groups and permissions, to avoid the relational clash with auth.User,
    # which complicates the model.
    # CASCADE delete, reasoning, GDPR right to forget (unless a business reason exists to keep the data).
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    # This is an event user, we associate this user with one or more events.
    events = models.ManyToManyField(Event, help_text=_("Event to choose."))

    def __str__(self) -> str:
        """
        Representation of this object.
        :return:
        """
        event_number = self.events.count()
        return f"{event_number} event{pluralize(event_number)} entered by {self.user.get_full_name()} " \
               f"({self.user.email})"

