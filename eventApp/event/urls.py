# Event URLS

from django.urls import path
from django.views.generic import TemplateView
from event.views import EventListView, EventEntryView, EventLoginView

urlpatterns = [
    path('events/entry/completed/', TemplateView.as_view(template_name="event/completed.html"),
         name='event-entry-completed'),
    # You could use a slug field here, sticking to ID for demo purposes.
    path('events/<int:id>/<str:email>/entry/', EventEntryView.as_view(), name='event-entry'),
    path('events/<int:id>/login/', EventLoginView.as_view(), name='event-entry-login'),
    path('events/', EventListView.as_view(), name='event-landing')
]
