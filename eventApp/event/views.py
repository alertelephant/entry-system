from typing import Any, Union

import django_redis
from django.contrib.auth import authenticate, login
from django.contrib.auth import get_user_model
from django.conf import settings
from django.shortcuts import get_object_or_404, redirect

# We can use async coroutines if we wish (we are running under an ASGI) but they as yet do not support
# key parts of Django like the ORM.
# You will get a SynchronousOnlyOperation exception
from django.urls import reverse_lazy

from django.views.generic.list import ListView
from django.views.generic.edit import FormView

from event.forms import LoginForm, EventEntryForm
from event.models import Event
from django.http import HttpResponseForbidden, HttpResponseRedirect

conn_rd2 = django_redis.get_redis_connection("redis_2")
conn_rd3 = django_redis.get_redis_connection("redis_3")


class EventListView(ListView):
    """
    Event list view
    """
    # Django now auto-links ListView to a template with the name format <model>_list.html
    # Thus this links to event_list.html

    model = Event


class EventEntryView(FormView):
    """
    Entry form processor for the event - full entry - new account.
    """
    # We cannot use the CreatView generic here since we are spanning two models.

    template_name = 'event/event_entry_form.html'
    form_class = EventEntryForm
    entryObj = None
    user_id = None

    # reverse(...) cannot be used in class parameters since it evaluates before the Django engine, has parsed the
    # URLs.
    success_url = reverse_lazy('event-entry-completed')

    def auth_user(self, request, **kwargs) -> Union[None, HttpResponseForbidden, HttpResponseRedirect]:
        """
        Recover the user object if one is available and authenticates it setting self.user_id.
        If there is an issue an appropriate http response is returned, else None
        :param request:
        :param kwargs:
        :return:
        """
        user_model = get_user_model()
        try:
            user = user_model.objects.get(email=kwargs["email"])

            # Security
            if request.user.is_authenticated and user.email != request.user.email:
                return HttpResponseForbidden()
            elif not request.user.is_authenticated:
                return redirect('event-entry-login', id=self.entryObj.id)
            self.user_id = user.id
        except user_model.DoesNotExist:
            pass

        return None

    def get(self, request, *args, **kwargs):
        """
        Overridden to set the entry object or call 404
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        # User exists in Redis? If so they are still being processed!
        # Scalability note - this is poor to scale, this needs replacing with a cluster and a single entry point.
        if conn_rd2.hget(settings.REDIS_LIST_PROCESS_KEY, kwargs.get("email")) or conn_rd3.hget(
                settings.REDIS_LIST_PROCESS_KEY, kwargs.get("email")):
            # Redirect to the "still being processed" page
            return redirect('in_processing')
        self.entryObj = get_object_or_404(Event, id=kwargs["id"])
        user_resp = self.auth_user(request, **kwargs)

        if user_resp:
            return user_resp

        return super(EventEntryView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs) -> Any:
        """
        Overridden to set the entry object or call 404
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        self.entryObj = get_object_or_404(Event, id=kwargs["id"])
        user_resp = self.auth_user(request, **kwargs)

        if user_resp:
            return user_resp

        return super(EventEntryView, self).post(request, *args, **kwargs)

    def get_form_kwargs(self):
        """
        Pass over the user's email, event ID and user_id (for a logged in user).
        :return:
        """
        kwargs = super(EventEntryView, self).get_form_kwargs()
        kwargs.update({
            'event_id': self.entryObj.id,
            'email': self.kwargs.get("email"),
            'user_id': self.user_id
        })
        return kwargs

    def get_context_data(self, **kwargs):
        """
        Provides the event object
        :param kwargs:
        :return:
        """
        context = super(EventEntryView, self).get_context_data(**kwargs)
        context['event'] = self.entryObj
        return context
    
    def form_valid(self, form):
        """
        Process the form.
        :param form: 
        :return: 
        """
        
        # That's all we have to do!
        form.save()
        
        return super(EventEntryView, self).form_valid(form)
        

class EventLoginView(FormView):
    """
    Login to the system.
    """
    form_class = LoginForm
    template_name = 'event/login.html'

    def get_form_kwargs(self):
        """
        Signals to the form which field to show and which one to hide.
        :return:
        """
        kwargs = super(EventLoginView, self).get_form_kwargs()

        # Example of "Truthy" self.request.POST.get("email") will return a string if it exists,
        # which is dynamically cast to boolean.
        kwargs['password_req'] = bool(self.request.POST.get("email", False))
        return kwargs

    def get_context_data(self, **kwargs):
        """
        Provides the state for login, if to provide email or password.
        :param kwargs:
        :return:
        """
        context = super(EventLoginView, self).get_context_data(**kwargs)

        if self.request.method == "GET":
            # Initial load - so email is being requested.
            context["email"] = True
        return context

    def form_valid(self, form):
        """
        This will redirect to event entry for a new user, or authenticate for an exiting user, then redirect to event
        entry.
        :param form:
        :return:
        """

        # This can intercept on the first post or second post.
        data = form.cleaned_data

        user_model = get_user_model()

        try:
            user = user_model.objects.get(email=data['email'])
        except user_model.DoesNotExist:
            user = None

        if user and data['password']:
            # Password provided so we go for authentication (user will exist in DB).
            user = authenticate(username=user.username, password=data["password"])
            if user is None:
                # Authentication failed - for demo purposes we will simple return unauthorised.
                return HttpResponseForbidden()
            else:
                # Login the user and then re-direct.
                login(self.request, user)
                return redirect('event-entry', email=user.email, id=self.kwargs["id"])
        elif user:
            # Need to return to the form to capture the password, so we invalidate the form.
            # The form will handle switching fields.
            return self.form_invalid(form)

        # New email, direct to the event entry for on-boarding.
        return redirect('event-entry', email=data['email'], id=self.kwargs["id"])
