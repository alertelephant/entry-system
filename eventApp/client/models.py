from django.db import models
from django.utils.translation import gettext as _
from django.conf import settings
from project.utils import AddressMixin, PhoneMixin

# Models that represent a client.


class ClientUser(AddressMixin, PhoneMixin, models.Model):
    """
    A client of the event system.
    """

    # We are adding to the user database so we are *not* going to use a proxy model.
    # Ideally for security we would want to implement a separate model inheriting from AbstractUser,
    # to keep these clients out of the admin part.
    # But we would then have to define groups and permissions, to avoid the relational clash with auth.User,
    # which complicates the model.
    # CASCADE delete, reasoning, GDPR right to forget (unless a business reason exists to keep the data).
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, help_text=_("User nominated as a "
                                                                                                "client."))

    # Field name: Could be a business or a charity, hence "organisation".
    # How big, we don't know for sure, the longest name for a company may be this one (which is 85 characters):
    # "MR NEVILLE JOHNSON & MRS SOPHIE JOHNSON (THE FOX WALMLEY) SUTTON COLDFIELD UK LIMITED" !
    organisation_name = models.TextField(help_text=_("Name of the client's organisation."))

    def __str__(self) -> str:
        """
        Representation of this object
        :return:
        """

        return f"{self.organisation_name}"
