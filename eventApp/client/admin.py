from django.contrib import admin

# Client admin
from client.models import ClientUser


@admin.register(ClientUser)
class ClientAdmin(admin.ModelAdmin):

    raw_id_fields = [
        "user"
    ]
