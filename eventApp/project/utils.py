# Utilities.
from django.core.validators import RegexValidator
from django.db import models


class AddressMixin(models.Model):
    """
    An address mixin to provide address fields to models that require recording this information.
    """

    address_1 = models.CharField(max_length=1024, help_text="Address line 1")
    address_2 = models.CharField(max_length=1024, help_text="Address line 2", null=True, blank=True)
    town_city = models.CharField(max_length=1024, help_text="Town or city")
    # Current longest international zip/post code is 10 characters.
    zip_postcode = models.CharField(max_length=10, help_text="Zipcode / postcode.")

    class Meta:
        abstract = True


class PhoneMixin(models.Model):
    """
    Telephone field mixin.
    """

    # During research on ITU-T E.164 found this nice regex for telephone numbers.
    telephone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: "
                                                                       "'+999999999'. Up to 15 digits allowed.")
    telephone = models.CharField(validators=[telephone_regex], max_length=17, blank=True)

    class Meta:
        abstract = True
